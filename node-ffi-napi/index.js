
const start = process.hrtime();
const ffi=require("ffi-napi");
const adder = ffi.Library("../src/add.so", {
	"add": ["int", ["int", "int"]]
});
const mid = process.hrtime();
const added = adder.add(4,5);
const end = process.hrtime();

console.log(added);

const load_time = (mid[0] * 1000000000 + mid[1] - (start[0] * 1000000000 + start[1])) / 1000;
const run_time = (end[0] * 1000000000 + end[1] - (mid[0] * 1000000000 + mid[1])) / 1000;
console.log(" Load library: " + load_time.toString() + "μs");
console.log("Function call: " + run_time.toString() + "μs");

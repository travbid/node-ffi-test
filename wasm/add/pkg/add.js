/* tslint:disable */
import * as wasm from './add_bg';
// const wasm = require('./add_bg.wasm');

/**
* @param {number} arg0
* @param {number} arg1
* @returns {number}
*/
export function add(arg0, arg1) {
    return wasm.add(arg0, arg1);
}


{
    "targets": [
        # {
        #     "target_name": "add.so",
        #     "sources": ["../src/add.cpp"],
        #     "include_dirs": [
        #         "../src"
        #     ],
        #     "cflags": [
        #         "-std=c++17 -fPIC -O3"
        #     ],
        #     "cflags_cc!": [
        #         "-fno-rtti",
        #         "-fno-exceptions"
        #     ]
        # },
        {
            "target_name": "adder",
            "sources": ["nnao.cpp"],
            "include_dirs": ["../src"],
            "link_settings": {
                "libraries": ["-l:add.so"],
                "library_dirs": ["../../src"],
                "ldflags": ["-Wl,-rpath,../src"]
            },
            "cflags": ["-std=c++17", "-O3"],
            "cflags_cc!": [
                "-fno-rtti",
                "-fno-exceptions"
            ]
        }
    ],

}

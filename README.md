```bash
cd src
clang++ -O3 -fPIC -c -o add.o add.cpp
clang++ -O3 -fPIC -shared -o add.so add.cpp
```

Then in each folder:

```bash
yarn install
node index.js
```


Results: (Gigabyte Aero 14, i7-7700HQ, Ubuntu 18.04, node v8.10.0)
```
Node Native Addons:
first call: (node index.js)
 Load library: ~2,990μs
Function call: ~12μs

subsequent calls:
 Load library: ~480μs
Function call: ~10μs

N-API:
first call:
 Load library: ~2,200μs
Function call: ~18μs

subsequent calls:
 Load library: ~900μs
Function call: ~15μs

node-ffi-napi:
first call:
 Load library: ~51,000μs
Function call: ~87μs

subsequent calls:
 Load library: ~16,000μs
Function call: ~85μs

wasm:
first call:
 Load library: ~1,200μs
Function call: ~1.6μs

subsequent calls:
 Load library: ~530μs
Function call: ~1.5μs
```

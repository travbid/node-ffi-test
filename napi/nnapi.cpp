
#include "add.hpp"
#include <node_api.h>

// #include<iostream>
// #include<chrono>

// using std::cout;
// using std::endl;

// using hrclock = std::chrono::high_resolution_clock;
// using TimePoint = std::chrono::time_point<hrclock>;
// using TimeDuration = std::chrono::duration<int64_t, std::chrono::milliseconds>;
// TimePoint time_now() { return std::chrono::high_resolution_clock::now(); }

napi_value Add(napi_env env, napi_callback_info info) {

	napi_status status;
	size_t argc = 2;
	napi_value argv[argc];
	// auto t1 = time_now();
	status = napi_get_cb_info(env, info, &argc, argv, nullptr, nullptr);
	
	if (status != napi_ok) { napi_throw_error(env, NULL, "Failed to parse arguments"); }

	int a, b;

	status = napi_get_value_int32(env, argv[0], &a);
	if (status != napi_ok) { napi_throw_error(env, NULL, "Invalid number was passed as argument"); }

	status = napi_get_value_int32(env, argv[1], &b);
	if (status != napi_ok) { napi_throw_error(env, NULL, "Invalid number was passed as argument"); }

	int res = add(a, b);
	// auto t2 = time_now();
	// std::chrono::nanoseconds dt = (t2 - t1);
	// status = napi_create_int32(env, static_cast<int32_t>(dt.count()/1000), &added);
	napi_value added;
	status = napi_create_int32(env, res, &added);

	if (status != napi_ok) { napi_throw_error(env, NULL, "Unable to create return value"); }

	return added;
}

napi_value Init(napi_env env, napi_value exports) {
	// Module initialization code goes here
	napi_status status;
	napi_value fn;

	// Arguments 2 and 3 are function name and length respectively
	// We will leave them as empty for this example
	status = napi_create_function(env, NULL, 0, Add, NULL, &fn);
	if (status != napi_ok) { napi_throw_error(env, NULL, "Unable to wrap native function"); }

	status = napi_set_named_property(env, exports, "add", fn);
	if (status != napi_ok) { napi_throw_error(env, NULL, "Unable to populate exports"); }

	return exports;
}

NAPI_MODULE(NODE_GYP_MODULE_NAME, Init)
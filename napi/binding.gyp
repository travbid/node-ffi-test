{
    "targets": [
        # {
        # 	"target_name": "add.so",
        # 	"sources": [ "add.cpp" ],
        # 	"include_dirs": [
        #         "../src"
        #     ],
        # 	"cflags": [
        # 		"-std=c++17 -fPIC -O3"
        # 	],
        # 	"cflags_cc!": [
        # 		"-fno-rtti",
        # 		"-fno-exceptions"
        # 	]
        # },
        {
            "target_name": "adder",
            # "dependencies": ["add.so"],
            "sources": ["nnapi.cpp"],
			"include_dirs": ["../src"],
            "link_settings": {
                "libraries": ["-l:add.so"],
                "library_dirs": ["../../src"],
                "ldflags": ["-Wl,-rpath,../src"],
            },
            "cflags": ["-std=c++17", "-O3"],
            "cflags_cc!": [
                "-fno-rtti",
                "-fno-exceptions"
            ]
        }
    ],
}
